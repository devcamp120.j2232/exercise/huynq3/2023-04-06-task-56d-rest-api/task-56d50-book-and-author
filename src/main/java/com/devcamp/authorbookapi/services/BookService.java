package com.devcamp.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.models.Book;

@Service
public class BookService extends AuthorService{
    Book book1 = new Book("book1", getAuthorBook1(), 1000, 2);
    Book book2 = new Book("book2", getAuthorBook2(), 1100, 2);
    Book book3 = new Book("book3", getAuthorBook3(), 1200, 3);
    Book book4 = new Book("book4", getAuthorBook4(), 1300, 3);
    Book book5 = new Book("book5", getAuthorBook5(), 1400, 4);
    Book book6 = new Book("book6", getAuthorBook6(), 1500, 4);
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        bookList.add(book4);
        bookList.add(book5);
        bookList.add(book6);
        return bookList;
    }
    public Book getBookById(int index){
        Book book = null;
        if (index >= 0 && index < getAllBooks().size()){
            book = getAllBooks().get(index);
        }
        return book;
    }
}
